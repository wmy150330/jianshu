/**
 * Created by imac13 on 19/1/1.
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'

class Witer extends PureComponent {
    render () {
        const {loginStatus} = this.props;
        if (loginStatus) {
            return(
                <div>写文章啦~</div>
            )
        }else {
            return <Redirect to='/login' />
        }

    }

}

const mapState = (state) => ({
    loginStatus:state.getIn(['login','login'])
});

export default connect(mapState,null)(Witer);