/**
 * Created by imac13 on 18/12/31.
 */
import styled from 'styled-components';

export const HomeWrapper = styled.div`
    width: 960px;
    overflow:hidden;
    margin: 0 auto;
`;
export const HomeLeft = styled.div`
    float:left;
    width: 625px;
    padding-top:30px;
    margin-left:15px;
    .banner-img{
        width: 625px;
        height: 270px;
    }
`;
export const HomeRight = styled.div`
    float:right;
    width: 280px;
    padding-top:30px;
`;
export const TopicWrapper = styled.div`
    padding: 20px 0 10px 0;
    overflow: hidden;
    margin-left: -18px;
    border-bottom: 1px solid #dcdcdc;
`;
export const TopicItem = styled.div`
    float:left;
    height: 32px;
    padding-right:10px;
    margin-left: 18px;
    margin-bottom: 18px;
    line-height: 32px;
    background: #f7f7f7;
    font-size: 14px;
    color: #000;
    border:1px solid #dcdcdc;
    border-radius: 4px;
    .item-img{
      float:left;
      width: 32px; 
      height: 32px;
      background: pink;
      margin-right:10px;
    }
`;
export const ListItem = styled.div`
    padding: 20px 0;
    overflow: hidden;
    border-bottom: 1px solid #dcdcdc;
    .listPic{
        float:right;
        display: block;
        width: 125px;
        height: 100px;
        border-radius: 10px;
    }
`;
export const ListInfo = styled.div`
    width: 500px;
    float:left;
    .title{
        line-height: 27px;
        font-size: 18px;
        font-weight: 700px;
        color: #333;
    }
    .desc{
        line-height: 24px;
        font-size: 14px;
        color: #999;
    }
`;
export const RecommendWrapper = styled.div`
    width: 280px;
    margin-top:-4px;
    background:#eee
`;
export const RecommendItem = styled.div`
    width: 280px;
    height:50px;
    background:url(${(props)=>props.imgUrl});
    background-size: contain;
`;
export const RecommendPic = styled.div`
    margin-top: 10px;
    margin-bottom: 30px;
    padding: 10px 22px;
    border: 1px solid #f0f0f0;
    border-radius: 6px;
    background-color: #fff;
    width: 280px;
    overflow:hidden;
    box-sizing: border-box;
    .rec-img{
        float:left;
        width: 60px;
        height:60px;
        background:green;
    }
`;
export const RecommendInfo = styled.div`
    margin-top: 12px;
    display: inline-block;
    vertical-align: middle;
    margin-left: 7px;
    .title{
        font-size: 15px;
        color: #333;
    }
    .desc{
        margin-top: 4px;
        font-size: 13px;
        color: #999;
    }
`;
export const WriterWrapper = styled.div`
    width: 280px;
    height:300px;
    border: 1px solid #f0f0f0;
`;
export const LoadMore = styled.div`
    width: 100%;
    height: 40px;
    line-height: 40px;
    margin: 30px 0;
    background: #a5a5a5;
    color: #fff;
    border-radius: 20px;
    cursor: pointer;
    text-align: center;
`;
export const BackTop = styled.div`
    position: fixed;
    right: 80px;
    bottom: 30px;
    width: 60px;
    height: 60px;
    line-height: 60px;
    text-align: center;
    font-size: 12px;
    border:1px solid #ccc;
`;