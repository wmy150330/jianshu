/**
 * Created by imac13 on 18/12/31.
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {RecommendWrapper,RecommendItem,RecommendPic,RecommendInfo} from '../style'

class Recommend extends PureComponent {
    render () {
        return(
            <div>
                <RecommendWrapper>
                    {
                        this.props.list.map((item) => {
                            return (
                                <RecommendItem imgUrl={item.get('imgURL')} key={item.get('id')}/>
                            )
                        })
                    }
                </RecommendWrapper>
                <RecommendPic>
                    <img className="rec-img" src="http://cdn2.jianshu.io/assets/web/download-index-side-qrcode-cb13fc9106a478795f8d10f9f632fccf.png" alt=""/>
                    <RecommendInfo>
                        <h3 className="title">下载简书手机App</h3>
                        <p className="desc">随时随地发现和创作内容</p>
                    </RecommendInfo>
                </RecommendPic>
            </div>
        )
    }
}

const mapState = (state)=>{
    return{
        list:state.getIn(['home','recList'])
    }
};
export default connect(mapState,null)(Recommend);