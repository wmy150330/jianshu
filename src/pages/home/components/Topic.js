/**
 * Created by imac13 on 18/12/31.
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {TopicWrapper,TopicItem} from '../style'

class Topic extends PureComponent {
    render () {
        return(
            <TopicWrapper>
                {
                    this.props.list.map((item) => {
                        return(
                            <TopicItem key = {item.get('id')}>
                                <div className="item-img"></div>
                                {item.get('title')}
                            </TopicItem>
                        )
                    })
                }

            </TopicWrapper>
        )
    }
}

const mapState = (state)=>{
    return{
        list:state.getIn(['home','topicList'])
    }
};
export default connect(mapState,null)(Topic);