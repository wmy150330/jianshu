/*Created by mengyun wang on 19/1/1*/
import axios from 'axios'
import {actionTypes} from './index'
import { fromJS } from 'immutable'

const changeHomeData = (result)=>{
    return{
        type: actionTypes.CHANGE_HOME_DATA,
        topicList: result.topicList,
        articleList: result.articleList,
        recList: result.recList,
    }
};
const addHomeList = (list,nextPage)=>{
    return{
        type: actionTypes.ADD_HOME_LIST,
        list:fromJS(list),
        nextPage
    }
};

export const getHomeInfo = () => {
    return (dispatch) => {
        axios.get('/api/home.json').then((res) => {
            const result = res.data.data;
            dispatch(changeHomeData(result));
        })
    }
};
export const getMoreList = (page) => {
    return (dispatch) => {
        axios.get('/api/homeList.json?page' + page).then((res) => {
            const result = res.data.data;
            dispatch(addHomeList(result,page+1));
        })
    }
};
export const toggleTopShow = (show)=>({
        type: actionTypes.TOGGLE_SHOW,
        show
});