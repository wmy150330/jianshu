/**
 * Created by imac13 on 18/12/31.
 */
import React, { Component } from 'react';
import {HomeWrapper,HomeLeft,HomeRight} from './style'
import Topic from './components/Topic';
import List from './components/List';
import Recommend from './components/Recommend';
import Writer from './components/Writer';
import { connect } from 'react-redux';
import {actionCreators} from './store';
import {BackTop} from './style'


class Home extends Component {
    handleScrollTop(){
        window.scrollTo(0,0)
    }
    render() {
        return (
            <HomeWrapper>
                <HomeLeft>
                    <img className="banner-img"
                         src="//upload.jianshu.io/admin_banners/web_images/4590/7f1edd154f90446a038d6ecd10bebf6e8929fbf5.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/1250/h/540"
                         alt=""/>
                    <Topic/>
                    <List/>
                </HomeLeft>
                <HomeRight>
                    <Recommend/>
                    <Writer/>
                </HomeRight>
                {
                    this.props.showScroll ? <BackTop onClick={this.handleScrollTop}>回到顶部</BackTop> : null
                }
            </HomeWrapper>
        )
    }

    componentDidMount() {
        this.props.changeHomeData();
        this.bindEvents()
    }
    componentWillUnmount () {
        window.removeEventListener('scroll',this.props.changeScroll)
    }
    bindEvents(){
        window.addEventListener('scroll',this.props.changeScroll)
    }
}

const mapState = (state) => ({
    showScroll:state.getIn(['home','showScroll'])
});
const mapDispatch = (dispatch) => ({
    changeHomeData () {
        dispatch(actionCreators.getHomeInfo())
    },
    changeScroll () {
        if (document.documentElement.scrollTop > 100) {
            dispatch(actionCreators.toggleTopShow(true))
        }else{
            dispatch(actionCreators.toggleTopShow(false))
        }
    }
});

export default connect(mapState,mapDispatch)(Home);