/**
 * Created by imac13 on 18/12/31.
 */
import React, { PureComponent } from 'react';
import { actionCreators } from './store';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import {DetailWrapper,Header,Content} from './style'


class Detail extends PureComponent {
    render () {
        return(
            <DetailWrapper>
                <Header>{this.props.title}</Header>
                <Content dangerouslySetInnerHTML={{__html: this.props.content}} />
            </DetailWrapper>
        )
    }
    componentDidMount () {
        this.props.getDetail(this.props.match.params.id);
    }
}

const mapState = (state) => ({
    title:state.getIn(['detail','title']),
    content:state.getIn(['detail','content'])
});
const mapDispatch = (dispatch) => {
    return {
        getDetail (id) {
            dispatch(actionCreators.getDetail(id));
        }
    }
}


export default connect(mapState,mapDispatch)(withRouter(Detail));