/**
 * Created by imac13 on 19/1/2.
 */
import Loadable from 'react-loadable';
import React from 'react';

const LoadableComponent = Loadable({
    loader: () => import('./'),
    loading () {
        return <div>正在拼命加载中.....</div>
    },
});

export default ()=> <LoadableComponent/>
