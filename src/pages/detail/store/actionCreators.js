/**
 * Created by imac13 on 19/1/1.
 */
import axios from 'axios';
import { actionTypes } from './index'

const getDetailData = (title,content) => ({
    type: actionTypes.GET_DETAIL_DATA,
    title,
    content
});
export const getDetail = (id) => {
    return (dispatch)=> {
        axios.get('/api/detail.json?id=' + id).then((res)=>{
            const result = res.data.data;
            dispatch(getDetailData(result.title,result.content))
        })
    }
};