/**
 * Created by imac13 on 18/12/27.
 */
import styled from 'styled-components'
import logoPic from '../../statics/logo.png'

export const HeaderWrapper = styled.div`
    position: relative;
    height:56px;
    border-bottom: 1px solid #eee;
    z-index:1;
`;
export const Logo = styled.div`
    position: absolute;
    top:0;
    left:0px;
    display:block;
    width: 100px;
    height: 56px;
    background:url(${logoPic});
    background-size: 100% 100%;
`;
export const Nav = styled.div`
    width: 960px;
    height: 100%;
    margin: 0 auto;
    box-sizing: border-box;
`;
export const NavItem = styled.div`
    height: 56px;
    line-height: 56px;
    padding: 0 15px;
    font-size:17px;
    color:#333;
    &.left{
        float: left;
    }
    &.right{
        float: right;
        color:#969696;
    }
    &.active{
        color:#ea6f5a;
    }
    
`;
export const SearchWrapper = styled.div`
    position: relative;
    float: left;
    .slide-enter {
        transition: all .2s ease-out;
    }
    .slide-enter-active {
        width: 240px;
    }
    .slide-exit {
        transition: all .2s ease-out;
    }
    .slide-exit-active {
        width: 160px;
    }
    .search{
        position: absolute;
        right: 20px;
        bottom: 4px;
        width: 30px;
        line-height: 30px;
        border-radius: 15px;
        text-align: center;
        &.focused {
            color:#fff;
            background:#888;
        }
    }
`;
export const NavSearch = styled.input.attrs({
    placeholder:'搜索'
})`
    width: 160px;
    height: 38px;
    margin-top: 9px;
    margin-left: 15px;
    padding: 0 35px 0 20px;
    box-sizing: border-box;
    border:none;
    outline:none;
    border-radius: 19px;
    background:#eee;
    margin-right: 14px;
    color:#666;
    &::placeholder {
       color:#999; 
    } 
    &.focused {
       width: 240px;
    }
`;
export const SearchInfo = styled.div`
    position: absolute;
    top:56px;
    left: 0;
    width:240px;
    padding: 0 20px;
    box-shadow: 0 0 8px rgba(0,0,0,.2);
    background: #fff;
`;
export const SearchInfoTitle = styled.div`
    margin-top: 20px;
    margin-bottom: 15px;
    line-height: 20px;
    font-size: 14px;
    color: #969696;
`;
export const SearchInfoSwitch = styled.span`
    float:right;
    font-size: 13px;
    cursor: pointer;
    .spin{
        display: block;
        float: left;
        font-size:12px;
        margin-right:2px;
        transition: all .2s ease-in;
        transform-origin: center center;
    }
`;
export const SearchInfoList = styled.div`
    overflow:hidden;
`;
export const SearchInfoItem = styled.a`
    display: block;
    float:left;
    margin-right: 10px;
    margin-bottom: 10px;
    line-height: 20px;
    padding: 0 5px;
    font-size: 12px;
    border: 1px solid #dddddd;
    color: #787878;
    border-radius: 3px;
`;
export const Action = styled.div`
    position: absolute;
    top:0;
    right: 0;
    height: 56px;
`;
export const ActionItem = styled.div`
    height: 38px;
    line-height: 38px;
    border-radius: 19px;
    margin-top: 9px;
    margin-right: 15px;
    font-size: 15px;
    float: right;
    padding:0 20px;
    border: 1px solid rgba(236,97,73,.7);
    &.reg{
        color: #ea6f5a;
    }
    &.writing{
        color: #fff;
        background-color: #ea6f5a;
    }
`;