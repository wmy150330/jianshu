/**
 * Created by imac13 on 18/12/28.
 */
import {actionTypes} from './index'
import axios from 'axios'
import { fromJS } from 'immutable'

const changeList = (data) => ({
    type:actionTypes.CHANGE_LIST,
    data:fromJS(data),
    totalPage:Math.ceil(data.length/10)
});
export const getFocusInput = () => ({
    type:actionTypes.CHANGE_FOCUS_INPUT
});
export const getBlurInput = () => ({
    type:actionTypes.CHANGE_BLUR_INPUT
});
export const mouseEnter = () => ({
    type:actionTypes.MOUSE_ENTER
});
export const mouseLeave = () => ({
    type:actionTypes.MOUSE_LEAVE
});
export const changePage = (page) => ({
    type:actionTypes.CHANGE_PAGE,
    page
});


export const getList = () => {
    return (dispatch)=> {
        axios.get('api/headerList.json').then((res) => {
            const data = res.data;
            dispatch(changeList(data.data));
        }).catch(() => {
            console.log('error')
        })
    }
};
