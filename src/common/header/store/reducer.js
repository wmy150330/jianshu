/**
 * Created by imac13 on 18/12/28.
 */
import {actionTypes} from './index'
import { fromJS } from 'immutable'

const defaultState = fromJS({
    focused:false,
    mouseIn:false,
    list:[],
    page:1,
    totalPage:1
});

export default (state = defaultState, action) => {
    switch (action.type){
        case actionTypes.CHANGE_FOCUS_INPUT :
            return state.set('focused',true);
        case actionTypes.CHANGE_BLUR_INPUT :
            return state.set('focused',false);
        case actionTypes.CHANGE_LIST :
            return state.merge({
                list:action.data,
                totalPage:action.totalPage
            });
        case actionTypes.MOUSE_ENTER :
            return state.set('mouseIn',true);
        case actionTypes.MOUSE_LEAVE :
            return state.set('mouseIn',false);
        case actionTypes.CHANGE_PAGE :
            return state.set('page',action.page);
        default:
            return state;
    }
}