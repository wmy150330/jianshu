/**
 * Created by imac13 on 18/12/28.
 */
export const CHANGE_FOCUS_INPUT = 'header/CHANGE_FOCUS_INPUT';
export const CHANGE_BLUR_INPUT = 'header/CHANGE_BLUR_INPUT';
export const CHANGE_LIST = 'header/CHANGE_LIST';
export const MOUSE_ENTER = 'header/MOUSE_ENTER';
export const MOUSE_LEAVE = 'header/MOUSE_LEAVE';
export const CHANGE_PAGE = 'header/CHANGE_PAGE';
